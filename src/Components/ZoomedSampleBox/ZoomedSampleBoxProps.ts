import { IBox } from "../../API/Interfaces/IBox";
import BoxProps from "../Utility/Box/BoxProps";

export default interface ZoomedSampleBoxProps extends Omit<BoxProps, "height" | "width" | "depth"> {
	box: IBox;
	rect: DOMRect;
	removeMe?: () => void;
	colors?: any;
}
