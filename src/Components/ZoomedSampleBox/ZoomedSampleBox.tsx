import React, { Component } from "react";
import { ISampleData } from "../../API/Interfaces/ISample";
import SampleBox from "../Fridge/SampleBox/SampleBox";
import styles from "./ZoomedSampleBox.module.css";
import ZoomedSampleBoxProps from "./ZoomedSampleBoxProps";

export default class ZoomedSampleBox extends Component<ZoomedSampleBoxProps> {

	static get HEIGHT() {
		return window.innerHeight * 0.08;
	}
	static get WIDTH() {
		return window.innerHeight * 0.1;
	}
	static get DEPTH() {
		return window.innerHeight * 0.1;
	}

	state: {
		samplesArray: ISampleData[][];
		zoomIn: boolean;
		hide: boolean;
	} = {
			samplesArray: [],
			zoomIn: false,
			hide: false,
		};

	componentDidMount() {
		requestAnimationFrame(() => {
			this.setState({
				zoomIn: true,
			});
		});
	}

	container = React.createRef<HTMLDivElement>();

	render() {

		const props = { ...this.props } as any;
		delete props.rect;
		delete props.className;
		delete props.style;
		delete props.transform;
		delete props.removeMe;

		return (
			<div
				className={
					styles.container + (this.state.hide ? " " + styles.hideContainer : "")
				}
				ref={this.container}
				onClick={this.handleContainerClick}>
				<SampleBox
					colors={this.props.colors}
					className={
						styles.box +
						(this.props.className ? " " + this.props.className : "") +
						(this.state.zoomIn ? " " + styles.zoomIn : "") +
						(this.state.hide ? " " + styles.hide : "")
					}
					style={{
						top: this.props.rect.top,
						left: this.props.rect.left,
						cursor: "auto",
						...this.props.style,
					}}
					{...props}
					depth={ZoomedSampleBox.DEPTH}
					width={ZoomedSampleBox.WIDTH}
					height={ZoomedSampleBox.HEIGHT}
					rotateable={true}
					customStyle={{
						top: {
							pointerEvents: "none",
						},
					}}
					renderTubes
					transform={`${
						this.state.zoomIn ? "rotateX(-60deg) scale3d(4, 4, 4)" : ""
						} ${this.props.transform ?? ""}`}
				></SampleBox>
			</div>
		);
	}

	handleContainerClick = (e: React.MouseEvent) => {
		if ((e.target as Node).isSameNode(this.container.current)) {
			this.setState({
				hide: true,
				zoomIn: false,
			});
			setTimeout(() => this.props.removeMe?.(), 500);
		}
	};
}
