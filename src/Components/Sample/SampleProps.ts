import { ISample, ISampleData } from "../../API/Interfaces/ISample";
import BoxProps from "../Utility/Box/BoxProps";

export default interface SampleProps extends BoxProps {
	sample?: ISample;
	sampleData: ISampleData;
	color?: any;
}
