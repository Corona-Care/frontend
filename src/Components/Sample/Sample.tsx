import React, { Component } from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import SampleTube from "../Utility/SampleTube/SampleTube";
import styles from "./Sample.module.css";
import SampleProps from "./SampleProps";

export default class Sample extends Component<SampleProps> {
	state: { hover: boolean } = {
		hover: false,
	};

	render() {
		const props = { ...this.props } as any;
		delete props.sampleData;
		delete props.transform;
		return (
			<OverlayTrigger
				trigger={["hover", "focus"]}
				popperConfig={{
					offset: [0, this.state.hover ? "-20px" : "-10px"],
				}}
				overlay={
					<Tooltip
						placement="top"
						style={{ marginBottom: "75px" }}
						id={this.props.sampleData.barcode}
					>
						{this.props.sampleData.barcode}
					</Tooltip>
				}
				onEnter={() => this.setState({ hover: true })}
				onExit={() => this.setState({ hover: false })}
			>
				<SampleTube
					{...props}
					width={this.props.width}
					height={this.props.height}
					topcolor={
						this.props.sampleData.is_removed ? "#000000" : this.props.color
					}
					style={{
						marginTop: this.state.hover ? "-10px" : "",
					}}
				/>
			</OverlayTrigger>
		);
	}
	static Container: React.FunctionComponent<React.HTMLProps<HTMLDivElement>> = (
		props
	) => {
		return (
			<div className={styles.container} {...props}>
				{props.children}
			</div>
		);
	};

	static Column: React.FunctionComponent = (props) => {
		return (
			<div className={styles.column} {...props}>
				{props.children}
			</div>
		);
	};

	static Row: React.FunctionComponent = (props) => {
		return (
			<div className={styles.row} {...props}>
				{props.children}
			</div>
		);
	};

	static Grid: React.FunctionComponent = (props) => {
		return (
			<div className={styles.grid} {...props}>
				{props.children}
			</div>
		);
	};
}
