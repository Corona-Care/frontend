import React, { Component } from "react";
import { Button, Nav, Navbar, OverlayTrigger, Tooltip } from "react-bootstrap";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import { ReactComponent as DownloadIcon } from "../../Assets/icons/Download.svg";
import { ReactComponent as UploadIcon } from "../../Assets/icons/Upload.svg";
import CsvModal from "../Utility/CsvModal/CsvModal";
import "./NavBar.css";
import styles from "./NavBar.module.css";
import NavBarProps from "./NavBarProps";
import Consts from "../../API/Consts";

export default withRouter(
	class NavBar extends Component<NavBarProps & RouteComponentProps> {
		state: { displayCsvModal: boolean } = { displayCsvModal: false };

		render() {
			return (
				<>
					<Navbar
						className={styles.container + " " + styles.blue}
						variant="dark"
						bg="dark">
						<Navbar.Brand className={styles.green} as={Link} to="/fridge">
							CoronaCare
            			</Navbar.Brand>
						<Navbar.Collapse>
							<Nav className="justify-content-center mr-auto">
								<Nav.Link
									className={styles.green}
									as={Link}
									to="/About"
									active={this.props.location.pathname === "/about"}
								>
									About
								</Nav.Link>
								<Nav.Link
									className={styles.green}
									as={Link}
									to="/search"
									active={this.props.location.pathname === "/search"}>
									Search
                				</Nav.Link>
								<Nav.Link
									className={styles.green}
									as={Link}
									to="/fridge"
									active={this.props.location.pathname === "/fridge" || this.props.location.pathname === "/"}>
									Fridge
                				</Nav.Link>
							</Nav>
							<OverlayTrigger
								trigger={["hover", "focus"]}
								placement="bottom"
								overlay={
									<Tooltip id="upload-csv-popup-toggle">Download CSV</Tooltip>
								}>
								<Button
									className={styles.uploadButton}
									variant="outline-dark">
									<a href={Consts.server + Consts.csvDownloadRoute}> <DownloadIcon /></a>

								</Button>
							</OverlayTrigger>
							<OverlayTrigger
								trigger={["hover", "focus"]}
								placement="bottom"
								overlay={
									<Tooltip id="upload-csv-popup-toggle">Upload CSV</Tooltip>
								}>
								<Button
									className={styles.uploadButton}
									variant="outline-dark"
									onClick={() =>
										this.setState({
											displayCsvModal: !this.state.displayCsvModal,
										})
									}>
									<UploadIcon />
								</Button>
							</OverlayTrigger>
						</Navbar.Collapse>
					</Navbar>
					<CsvModal
						visible={this.state.displayCsvModal}
						onClose={() => {
							this.setState({ displayCsvModal: false });
						}}
					/>
				</>
			);
		}
	}
);
