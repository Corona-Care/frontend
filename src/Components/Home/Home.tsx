import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import APIHandler from "../../API/Handler";
import styles from "./Home.module.css";

export default class Home extends Component {
	state: { homeData: Object; data: []; contributors: [] } = {
		data: [],
		homeData: {},
		contributors: [],
	};
	componentDidMount = async () => {
		let homeData = await APIHandler.getHome();
		let contributors;
		if (Object.keys(homeData).includes("Contributors")) {
			contributors = homeData["Contributors"];
		}
		this.setState({ homeData, contributors });
	};

	isValidUrl = (str: any) => {
		try {
			new URL(str);
		} catch (_) {
			return false;
		}

		return true;
	};

	render() {
		const { homeData, contributors } = this.state;
		return (
			<>
				<Modal.Dialog size="lg">
					<Modal.Header>
						<Modal.Title>Links and Contributors</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className={styles.linksDiv}>
							{Object.entries(homeData).map(([key, value]) => {
								return this.isValidUrl(value) ? (
									<a target="_blank" rel="noopener noreferrer" href={value} key={key}>
										{key}
									</a>
								) : null;
							})}
						</div>

						<div className={styles.creditsDiv}>
							Contributors:
                {Object.entries(contributors).map(([key, value], i) => {
							return (
								<a key={i} rel="noopener noreferrer" target="_blank" href={`mailto:${value}`}>{key}</a>
							);
						})}
              To Shaare Zedek Medical Center
            </div>
					</Modal.Body>
				</Modal.Dialog>
			</>
		);
	}
}
