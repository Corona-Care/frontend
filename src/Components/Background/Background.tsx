import React, { Component } from "react";
import styles from "./Background.module.css";

export default class Background extends Component {
	render() {
		return (
			<div className={styles.container}>
				<div className={styles.stars} />
				<div className={styles.stars2} />
				<div className={styles.stars3} />
			</div>
		)
	}
}
