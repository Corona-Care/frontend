import React, { Component } from "react";
//@ts-ignore
import Barcode from "react-barcode";
import { Card, Modal, Table, Spinner } from "react-bootstrap";
import APIHandler from "../../../API/Handler";
import { IPatientData } from "../../../API/Interfaces/IPaitent";
import { ISampleData } from "../../../API/Interfaces/ISample";
import { ISession } from "../../../API/Interfaces/ISession";
import styles from "./SampleModal.module.css";
import SampleModalProps from "./SampleModalProps";

export default class SampleModal extends Component<SampleModalProps> {
  state: {
    sampleData?: ISampleData;
    patientData?: IPatientData;
    currentSession?: ISession;
    loading: boolean;
  } = {
    loading: false,
  };

  componentDidUpdate = async (prevProps: any, prevState: any) => {
    if (prevProps.sampleCode !== this.props.sampleCode) {
      this.loadData();
    }
  };

  componentDidMount = async () => {
    this.loadData();
  };

  loadData = async () => {
    this.setState({ loading: true });
    const sampleData = this.props.sample;
    const patientData = await APIHandler.getPatientData(sampleData.study_num);

    const sessions = patientData.sessions;
    let currentSession = null;

    if (sessions) {
      sessions.forEach((session: ISession) => {
        let sessionsSamples = session.samples;

        Object.entries(sessionsSamples).forEach(([key, value]) => {
          let valueSample = value as ISampleData;
          if (valueSample.barcode === sampleData.barcode) {
            currentSession = session;
            return;
          }
        });
      });
    }
    this.setState({ sampleData, patientData, currentSession, loading: false });
  };

  render() {
    const props = { ...this.props };
    const { sampleData, patientData, currentSession } = this.state;
    return (
      <Modal
        show={props.visible}
        onHide={() => {
          this.setState({
            sampleData: null,
            patientData: null,
            currentSession: null,
          });
          this.props.onClose();
        }}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
        className={styles.sampleModal + " sampleModal"}
      >
        <Modal.Header closeButton>
          <Modal.Title
            className={styles.title}
            id="example-custom-modal-styling-title"
          >
            <div className={styles.barcode}>
              <Barcode
                background={"#ececec"}
                height={50}
                value={props.sampleCode}
              />
              {/* <QRCode size={100} value={props.sampleCode} /> */}
            </div>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {sampleData && (
            <>
              <Card.Title>Sample Info</Card.Title>
              <Table striped bordered hover>
                <tbody>
                  {Object.entries(sampleData).map(([key, value]) => {
                    return (
                      !["barcod", "box", "box_id", "box_size"].includes(
                        key
                      ) && (
                        <tr key={`${key}:${value}`}>
                          <td>{key}</td>
                          <td>{value !== null && value.toString()}</td>
                        </tr>
                      )
                    );
                  })}
                </tbody>
              </Table>
            </>
          )}

          {patientData && (
            <>
              <Card.Title>Patient Info</Card.Title>
              <Table striped bordered hover>
                <tbody>
                  {Object.entries(patientData).map(([key, value]) => {
                    return (
                      !["sessions", "study_num", "box", "cell"].includes(
                        key
                      ) && (
                        <tr key={`${key}:${value}`}>
                          <td>{key}</td>
                          <td>{value !== null && value.toString()}</td>
                        </tr>
                      )
                    );
                  })}
                </tbody>
              </Table>
            </>
          )}

          {currentSession && (
            <>
              <Card.Title>Session Info</Card.Title>
              <Table striped bordered hover>
                <tbody>
                  {Object.entries(currentSession).map(([key, value]) => {
                    return (
                      !["samples", "study_num", "box", "cell"].includes(
                        key
                      ) && (
                        <tr key={`${key}:${value}`}>
                          <td>{key}</td>
                          <td>{value !== null && value.toString()}</td>
                        </tr>
                      )
                    );
                  })}
                </tbody>
              </Table>
            </>
          )}

          {this.state.loading && (
            <div className={styles.spinnerDiv} hidden={!this.state.loading}>
              <Spinner
                className={styles.spinner}
                animation="border"
                role="status"
              />
            </div>
          )}
        </Modal.Body>
      </Modal>
    );
  }
}
