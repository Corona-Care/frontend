import React from "react";
import { ISampleData } from '../../../API/Interfaces/ISample';
export default interface SampleModalProps extends React.DetailedHTMLProps<React.HTMLProps<HTMLDivElement>, HTMLDivElement> {
	sampleCode: string;
	sample: ISampleData;
	visible: boolean;
	onClose: Function;
}