import React, { Component, ChangeEvent, MouseEvent } from "react";
import styles from "./CvsModal.module.css";
import CsvModalProps from "./CsvModalProps";
import { Modal, Spinner, Button } from "react-bootstrap";
import APIHandler from "../../../API/Handler";

export default class CsvModal extends Component<CsvModalProps> {
  state: {
    selectedFile?: File;
    fileName?: string;
    errorText: string;
    loading: boolean;
    resolved: boolean;
  } = {
    errorText: "",
    loading: false,
    resolved: false,
  };

  onClose = () => {
    if (!this.state.loading) this.props.onClose();
  };

  handleFileChosen = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.target.files[0]) {
      if (e.target.files[0].type === "text/csv") {
        this.setState({
          selectedFile: e.target.files[0],
          loaded: 0,
          fileName: e.target.files[0].name,
          errorText: "",
        });
      } else {
        this.setState({
          selectedFile: e.target.files[0],
          loaded: 0,
          fileName: e.target.files[0].name,
          errorText: "",
        });
      }
    }
  };

  handleBtnClick = async (e: MouseEvent) => {
    if (this.state.selectedFile) {
      this.setState({ loading: true });
      let result = await APIHandler.uploadCsv(this.state.selectedFile);
      this.setState({ loading: false });

      if (result) {
        this.setState({ resolved: true });
      }
    }
  };
  render() {
    const props = { ...this.props };

    return (
      <Modal
        show={props.visible}
        onHide={this.onClose}
        className="csv-modal"
        dialogClassName={"modal-90w" + styles.container}
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title
            className={styles.title}
            id="example-custom-modal-styling-title"
          >
            Upload a new RedCup csv file
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div hidden={this.state.loading || this.state.resolved}>
            <div className="input-group">
              <div className="custom-file">
                <input
                  accept=".csv"
                  type="file"
                  className="custom-file-input"
                  id="inputGroupFile01"
                  aria-describedby="inputGroupFileAddon01"
                  onChange={this.handleFileChosen}
                />
                <label className="custom-file-label" htmlFor="inputGroupFile01">
                  {this.state.fileName || "Choose File"}
                </label>
              </div>
            </div>
            <label>{this.state.errorText}</label>

            <Button
              className={styles.uploadbtn}
              onClick={this.handleBtnClick}
              disabled={this.state.fileName ? false : true}
            >
              Upload
            </Button>
          </div>
          <div className={styles.spinnerDiv} hidden={!this.state.loading}>
            <Spinner
              className={styles.spinner}
              animation="border"
              role="status"
            />
          </div>
          <label>
            {this.state.resolved
              ? `File upload ${
                  this.state.resolved ? "succeeded" : "failed"
                } for ${this.state.fileName}`
              : ""}
          </label>
        </Modal.Body>
      </Modal>
    );
  }
}
