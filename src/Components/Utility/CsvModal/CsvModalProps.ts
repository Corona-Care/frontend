import React from "react";

export default interface CsvModalProps extends React.DetailedHTMLProps<React.HTMLProps<HTMLDivElement>, HTMLDivElement> {
	visible: boolean;
	onClose: Function;
}