import React, { Component } from "react";
import styles from "./Box.module.css";
import BoxProps from "./BoxProps";

export default class Box extends Component<BoxProps> {

	frontRef = React.createRef<HTMLDivElement>();
	backRef = React.createRef<HTMLDivElement>();
	leftRef = React.createRef<HTMLDivElement>();
	rightRef = React.createRef<HTMLDivElement>();
	topRef = React.createRef<HTMLDivElement>();
	bottomRef = React.createRef<HTMLDivElement>();

	state: { rotationX: number, rotationY: number } = {
		rotationX: 0,
		rotationY: 0
	};

	render() {
		const props = { ...this.props };
		delete props.className;
		delete props.height;
		delete props.depth;
		delete props.width;
		delete props.transform;
		delete props.background;
		delete props.text;
		delete props.renderWithTrueDimensions;
		delete props.style
		delete props.customStyle;
		delete props.onMouseDown;
		delete props.onMouseUp;
		delete props.onMouseMove;
		delete props.rotateable;
		let compleBackground: any, simpleBackground;
		if (typeof this.props.background !== "string")
			compleBackground = this.props.background;
		else
			simpleBackground = this.props.background;
		return (
			<div className={styles.container + (this.props.className ? " " + this.props.className : "") +
				(this.props.rotateable ? " " + styles.draggable : "") +
				(this.lastMouseLocation.x ? " " + styles.dragging : "")} style={{
					"--height": this.props.height + "px",
					"--width": this.props.width + "px",
					"--depth": this.props.depth + "px",
					...this.props.style
				} as any} {...props}
				onMouseDown={this.handleMouseDown} onMouseUp={this.handleMouseUp} onMouseMove={this.handleMouseMove}>
				<div className={styles.box} style={{
					transform: this.state.rotationX || this.state.rotationY ?
						`rotateX(${this.state.rotationX}deg) rotateY(${this.state.rotationY}deg) ${this.props.transform ?? ""}` :
						this.props.transform ?? "",
					background: simpleBackground
				}}>
					{this.props.children}
					{this.props.renderWithTrueDimensions ? this.props.renderWithTrueDimensions(this.getTrueSizes()) : null}
					{
						['front', 'back', 'left', 'right', 'top', 'bottom'].map(side => (
							<div key={side} className={styles[side]} style={{
								background: compleBackground?.[side] || compleBackground?.all,
								...this.props.customStyle?.all,
								...(this.props.customStyle as any)?.[side]
							}} ref={(this as any)[side + 'Ref']}>
								{(this.props.text as any)?.[side] || this.props.text?.all}
							</div>
						))
					}
				</div>
			</div>
		)
	}
	getTrueSizes() {
		return {
			front: this.frontRef.current?.getBoundingClientRect(),
			back: this.backRef.current?.getBoundingClientRect(),
			left: this.leftRef.current?.getBoundingClientRect(),
			right: this.rightRef.current?.getBoundingClientRect(),
			top: this.topRef.current?.getBoundingClientRect(),
			bottom: this.bottomRef.current?.getBoundingClientRect()
		}
	}

	lastMouseLocation: { x?: number, y?: number } = { x: undefined, y: undefined };
	isDragginBox = false;

	handleMouseMove = (e: React.MouseEvent<HTMLDivElement>) => {
		this.props.onMouseMove?.(e);
		if (this.isDragginBox && this.props.rotateable) {
			if (!this.lastMouseLocation.x || !this.lastMouseLocation.y)
				this.lastMouseLocation = {
					x: e.clientX + 1,
					y: e.clientY + 1
				};
			this.setState({
				rotationY: this.state.rotationY - (this.lastMouseLocation.x! - e.clientX) / 2,
				rotationX: this.state.rotationX + (this.lastMouseLocation.y! - e.clientY) / 2
			});
			this.lastMouseLocation.x = e.clientX;
			this.lastMouseLocation.y = e.clientY;
		}
	}

	handleMouseDown = (e: React.MouseEvent<HTMLDivElement>) => {
		this.isDragginBox = true;
		this.props.onMouseUp?.(e);
		this.lastMouseLocation = { x: undefined, y: undefined };
	}
	handleMouseUp = (e: React.MouseEvent<HTMLDivElement>) => {
		this.props.onMouseUp?.(e);
		this.isDragginBox = false;
		this.lastMouseLocation = { x: undefined, y: undefined };
		this.forceUpdate();
	}
}