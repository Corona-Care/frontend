import React from "react";

export interface FacesInterface<T> {
	front?: T;
	back?: T;
	left?: T;
	right?: T;
	top?: T;
	bottom?: T;
	all?: T;
}

type BoxBackground = FacesInterface<string>;

type BoxText = FacesInterface<string>;
type TrueDimensions = Omit<FacesInterface<DOMRect>, "all">;
type CustomStyle = FacesInterface<React.CSSProperties>;

export default interface BoxProps extends React.DetailedHTMLProps<React.HTMLProps<HTMLDivElement>, HTMLDivElement> {
	depth: number;
	height: number;
	width: number;
	transform?: string;
	background?: BoxBackground;
	text?: BoxText;
	renderWithTrueDimensions?: (trueDimension: TrueDimensions) => React.ReactNode;
	customStyle?: CustomStyle;
	rotateable?: boolean;
}