import React from "react";
import { ISampleData } from "../../../API/Interfaces/ISample";

export default interface ResultsTableProps extends React.DetailedHTMLProps<React.HTMLProps<HTMLDivElement>, HTMLDivElement> {
	samples?: ISampleData[];
	onSampleClick: (sample: ISampleData) => void;
}