import React, { Component } from "react";
import { Table } from "react-bootstrap";
import styles from "./ResultsTable.module.css";
import ResultsTableProps from "./ResultsTableProps";

export default class ResultsTable extends Component<ResultsTableProps> {
	render() {
		const { samples } = this.props;
		if (samples && samples.length > 0)
			return (
				<Table striped bordered hover>
					<tbody>
						<tr>
							{
								Object.keys(samples[0]).map((key, i) => (
									<td key={i}>{key}</td>
								))
							}
						</tr>
						{
							samples.map((sample, i) => (
								<tr className={styles.clickableRow} key={i} onClick={() => this.props.onSampleClick(sample)}>
									{
										Object.values(sample).map((value, i) => (
											<td key={i}>
												{value?.toString() ?? ""}
											</td>
										))
									}
								</tr>
							))
						}
					</tbody>
				</Table>
			);
		return null;
	}
}
