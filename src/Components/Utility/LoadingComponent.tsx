import React, { Component } from "react";
import { Modal, Spinner } from "react-bootstrap";
export default function loadingComponent<T, S>(component: React.ComponentType<T & S>, didMountFunction: () => S): React.ComponentClass<T & S> {
	return class LoadingComponent extends Component<T & S> {
		state: { loading: boolean } = {
			loading: false
		}
		render() {
			if (this.state.loading)
				return (
					<div style={{
						position: "absolute"
					}}>
						<Modal.Dialog>
							<Modal.Header>
								Loading....
                            </Modal.Header>
							<Modal.Body>
								<Spinner animation="border" />
							</Modal.Body>
						</Modal.Dialog>
					</div>
				)
			return React.createElement(component, this.props);
		}
	}
}