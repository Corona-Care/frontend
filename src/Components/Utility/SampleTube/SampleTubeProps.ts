import React from "react";

export default interface SampleTubeProps extends React.DetailedHTMLProps<React.HTMLProps<HTMLDivElement>, HTMLDivElement> {
	height: number;
	width: number;
	transform?: string;
	rotateable?: boolean;
	topcolor?: string;
}