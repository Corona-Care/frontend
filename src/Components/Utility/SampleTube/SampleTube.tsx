import React, { Component } from "react";
import styles from "./SampleTube.module.css";
import SampleTubeProps from "./SampleTubeProps";

export default class SampleTube extends Component<SampleTubeProps> {
	render() {
		const props = { ...this.props };
		return (
			<>
				<div
					className={
						styles.scene +
						(this.props.className ? " " + this.props.className : "") +
						(this.props.rotateable ? " " + styles.draggable : "")
					}
					style={
						{
							...this.props.style,
						} as any
					}
					{...props}
				>
					<div
						className={styles.shape + " " + styles.cylinder + " " + styles.tube}
					>
						<div className={styles.face + " " + styles.bm}></div>
						<div
							className={styles.face + " " + styles.tp}
							style={
								this.props.topcolor
									? { backgroundColor: this.props.topcolor }
									: { backgroundColor: "red" }
							}
						></div>

						{new Array(14).fill(null).map((_, i) => (
							<div
								key={`sampleTubeBody${i}`}
								className={
									styles.face + " " + styles.side + " " + styles[`s${i}`]
								}
								style={
									this.props.topcolor
										? {
											background: `linear-gradient(to top, gray 85%, ${this.props.topcolor} 25%)`,
										}
										: {
											background:
												"linear-gradient(to top, white 85%, red 25%)",
										}
								}
							></div>
						))}
					</div>
				</div>
			</>
		);
	}
}
