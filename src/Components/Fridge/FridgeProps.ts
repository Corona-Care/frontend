import { IBox } from "../../API/Interfaces/IBox";
import { IFridge } from "../../API/Interfaces/IFridge";
import BoxProps from "../Utility/Box/BoxProps";


export interface FridgeProps2 extends Omit<BoxProps, "height" | "width" | "depth"> {
	fridgeData?: IFridge;
	onBoxClick?(event: React.MouseEvent, box: IBox, props: BoxProps): void;
	onShelfClick?(event: React.MouseEvent, boxees: (IBox | undefined)[], props: BoxProps): void;
}