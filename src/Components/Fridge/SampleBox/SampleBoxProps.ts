import { IBox } from "../../../API/Interfaces/IBox";
import BoxProps from "../../Utility/Box/BoxProps";

export default interface SampleBoxProps extends BoxProps {
	box: IBox;
	renderTubes?: boolean;
	colors: any;
	disableSampleClick?: boolean;
}
