import React, { Component } from "react";
import { ISample, ISampleData } from "../../../API/Interfaces/ISample";
import Sample from "../../Sample/Sample";
import Box from "../../Utility/Box/Box";
import SampleModal from "../../Utility/SampleModal/SampleModal";
import styles from "./SampleBox.module.css";
import SampleBoxProps from "./SampleBoxProps";

export default class SampleBox extends Component<SampleBoxProps> {
	static BOX_HEIGHT = 50;
	boxRef = React.createRef<HTMLDivElement>();

	static MAX_SAMPLE_DEPTH = 40;
	static MAX_SAMPLE_WIDTH = 20;
	static SAMPLE_HIGHT = 123;

	state: {
		samplesArray: ISampleData[][];
		modalVisible: boolean;
		selectedSample?: ISampleData;
	} = {
			samplesArray: [],
			modalVisible: false,
		};

	buildGridArray(samples: Object, size: number) {
		type OptionalSample = ISample | undefined;
		const height = Math.round(Math.sqrt(size)),
			width = height,
			samplesArray: OptionalSample[][] = [];

		function cellPosition(cell: number, width: number) {
			const y = parseInt((cell / 6).toString());
			let x = (cell % width) - 1;
			if (x === -1) x = width - 1;
			return { y, x };
		}

		for (let i = 0; i < width; i++) {
			let col = new Array<OptionalSample>(height).fill(undefined);
			samplesArray.push(col);
		}

		Object.values(samples || {}).forEach((value) => {
			let { x, y } = cellPosition(value.cell, width);
			samplesArray[x][y] = value;
		});

		this.setState({ samplesArray });
	}

	componentDidMount() {
		let box = this.props.box;
		this.buildGridArray(box.samples, box.size);
	}
	color = (type: string) => {
		if (this.props.colors) {
			return this.props.colors[type] ? this.props.colors[type] : "red";
		}

		return "red";
	};

	render() {
		const props = { ...this.props } as any;
		delete props.box;
		delete props.text;
		delete props.background;
		delete props.customStyle;
		delete props.children;
		delete props.className;
		delete props.renderTubes;
		delete props.disableSampleClick;
		return (
			<Box
				ref={this.boxRef}
				{...props}
				text={{ ...this.props.text, front: this.props.box.name }}
				className={
					styles.sampleBox +
					(this.props.className ? " " + this.props.className : "")
				}
				customStyle={{
					...this.props.customStyle,
					all: {
						border: "1px solid black",
						pointerEvents: "all",
						...this.props.customStyle?.all
					},
					top: {
						pointerEvents: "none",
						...this.props.customStyle?.top
					},
				}}
				background={{
					...this.props.background,
					all: "rgb(35, 35, 35)",
					top: "transparent",
				}}
				{...props}
			>
				{this.props.children}
				{
					<Sample.Container
						style={{
							display: this.props.renderTubes ? "" : "none",
						}}
					>
						{this.state.selectedSample && (
							<SampleModal
								visible={this.state.modalVisible}
								sampleCode={this.state.selectedSample.barcode.toString()}
								sample={this.state.selectedSample}
								onClose={() => this.setState({ modalVisible: false })}
							/>
						)}
						<Sample.Grid>
							{this.state.samplesArray.map((column, j) => {
								return (
									<Sample.Column key={j}>
										{column.map((sample, i) => {
											if (sample)
												return (
													<Sample.Row key={i}>
														{sample && (
															<Sample
																color={this.color(sample.type)}
																key={i}
																sampleData={sample}
																height={SampleBox.BOX_HEIGHT}
																depth={0}
																width={0}
																onClick={() => this.handleSampleClick(sample)}
															/>
														)}
													</Sample.Row>
												);
											return <div key={i} />;
										})}
									</Sample.Column>
								);
							})}
						</Sample.Grid>
					</Sample.Container>
				}
			</Box>
		);
	}

	handleSampleClick = (sampleCode: ISampleData) => {
		if (!this.props.disableSampleClick)
			this.setState({ selectedSample: sampleCode, modalVisible: true });
	};

	static Container: React.FunctionComponent<React.HTMLProps<HTMLDivElement>> = props => {
		if (props.className)
			delete props.className;
		return (
			<div {...props} className={styles.container + (props.className ? ' ' + props.className : '')}>
				{props.children}
			</div>
		);
	};

	static Column: React.FunctionComponent<React.HTMLProps<HTMLDivElement>> = props => {
		return (
			<div className={styles.column} {...props}>
				{props.children}
			</div>
		)
	}
}
