import { IBox } from "../../../API/Interfaces/IBox";

export default interface UnlocatedBoxesProps {
	boxes: IBox[];
	className?: string;
	onDragStart?: (box: IBox) => void;
	onDragEnd?: (box: number) => void;
	onDragEnter?: () => void;
	colors: any;
}
