import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import ZoomedSampleBox from "../../ZoomedSampleBox/ZoomedSampleBox";
import SampleBox from "../SampleBox/SampleBox";
import styles from "./UnlocatedBoxes.module.css";
import UnlocatedBoxesProps from "./UnlocatedBoxesProps";

export default class UnlocatedBoxes extends Component<UnlocatedBoxesProps> {
	render() {
		return (
			<Modal.Dialog className={this.props.className}>
				<Modal.Header>
					Unsorted Boxes
				</Modal.Header>
				<Modal.Body className={styles.grid}>
					{
						this.props.boxes.map((box, i) => (
							<SampleBox box={box}
								key={i}
								colors={this.props.colors}
								onDragStart={() => this.props.onDragStart?.(box)}
								onDragEnd={() => this.props.onDragEnd?.(i)}
								draggable
								height={ZoomedSampleBox.HEIGHT}
								width={ZoomedSampleBox.WIDTH}
								depth={ZoomedSampleBox.DEPTH} />
						))
					}
				</Modal.Body>
			</Modal.Dialog>
		)
	}
}
