import { IBox } from "../../../API/Interfaces/IBox";
import BoxProps from "../../Utility/Box/BoxProps";

export default interface ShelfProps extends BoxProps {
	boxesGrid: (IBox | undefined)[][];
	spaceBetweenBoxes?: number;
	onBoxClick?: (e: React.MouseEvent, box: IBox, props: BoxProps) => void;
	onShelfClick?: () => void;
	renderTubes?: boolean;
	onDragToColumnEnter?: (colIndex: number, rowIndex: number) => void;
	onBoxDragStart?: (box: IBox) => void;
	onBoxDragEnd?: () => void;
	colors?: any

}