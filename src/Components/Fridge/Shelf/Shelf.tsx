import React, { Component } from "react";
import Box from "../../Utility/Box/Box";
import SampleBox from "../SampleBox/SampleBox";
import styles from "./Shelf.module.css";
import ShelfProps from "./ShelfProps";

export default class Shelf extends Component<ShelfProps> {
	state = {
		isDraggin: false
	}

	render() {
		const props = { ...this.props } as any;
		delete props.boxesGrid;
		props.height /= 2;
		delete props.onBoxClick;
		delete props.className;
		delete props.onShelfClick;
		delete props.onDragToColumnEnter;
		delete props.onBoxDragStart;
		delete props.onBoxDragEnd;
		props.width = props.width / this.props.boxesGrid.length - (this.props.spaceBetweenBoxes ?? 10) / ((this.props.boxesGrid.length - 1) || 1);
		props.depth = props.depth / this.props.boxesGrid.length - (this.props.spaceBetweenBoxes ?? 10) / ((this.props.boxesGrid.length - 1) || 1);
		return (
			<div
				onClick={() => this.props.onShelfClick?.()}
				className={styles.wrapper +
					(this.props.className ? ' ' + this.props.className : '')}>
				<SampleBox.Container className={this.state.isDraggin ? styles.draggin : ''} style={{ height: this.props.depth }}>
					{
						this.props.boxesGrid.map((boxes, i) => {
							return (
								<SampleBox.Column key={i}>
									{
										boxes.map((box, j) => (
											!!box ? <SampleBox renderTubes={this.props.renderTubes}
												draggable
												colors={this.props.colors}
												onDragStart={() => this.props.onBoxDragStart?.(box)}
												onDragEnd={() => this.props.onBoxDragEnd?.()}
												className={styles.sampleBox}
												disableSampleClick
												text={{
													top: box.name,
													front: box.name
												}}
												background={{
													top: 'rgb(35 ,35 ,35)'
												}}
												customStyle={{
													top: {
														opacity: 0.3,
														pointerEvents: "all"
													}
												}}
												onClick={e => {
													e.stopPropagation();
													this.props.onBoxClick?.(e, box, props);
												}}
												key={j} {...props} box={box} />
												: <div key={j}
													onDragOver={e => e.preventDefault()}
													onDragEnter={() => this.props.onDragToColumnEnter?.(i, j)} />
										))
									}
								</SampleBox.Column>
							)
						})
					}
				</SampleBox.Container>
				<Box depth={this.props.depth} height={this.props.height / 4} width={this.props.width - 2}
					className={styles.shelfBottom}
					background={{
						all: "rgb(120, 120, 120)",
						top: "transparent",
						front: "rgba(120, 120, 120, 0.8)"
					}}
					customStyle={{
						all: {
							border: "1px solid black",
							pointerEvents: "all"
						},
						top: {
							border: "none",
							pointerEvents: "none"
						},
						front: {
							borderTop: "none"
						}
					}} transform={this.props.transform} />
			</div>
		)
	}

	static Container: React.FunctionComponent<React.DetailedHTMLProps<React.HTMLProps<HTMLDivElement>, HTMLDivElement>> = (props) => {
		const withoutClassName = { ...props };
		delete withoutClassName.className;
		return <div className={styles.container + (props.className ? " " + props.className : "")}
			{...withoutClassName}>{props.children}</div>
	}
}
