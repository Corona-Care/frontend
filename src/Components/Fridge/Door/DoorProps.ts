import React from "react";
export default interface DoorProps extends React.DetailedHTMLProps<React.HTMLProps<HTMLDivElement>, HTMLDivElement> {
	open: boolean;
	height: number;
	width: number;
}