import React, { Component } from "react";
import Box from "../../Utility/Box/Box";
import BoxProps from "../../Utility/Box/BoxProps";
import styles from "./Door.module.css";
import DoorProps from "./DoorProps";

export default class Door extends Component<DoorProps & Omit<BoxProps, "depth"> & { depth?: number }> {
	render() {
		const props = { ...this.props };
		delete props.open;
		delete props.className;
		delete props.onClick;
		delete props.transform;
		return (
			<Box className={styles.inner + (this.props.className ? " " + this.props.className : "")}
				depth={this.props.depth ?? 20} onClick={this.props.onClick}
				transform={`${this.props.transform ?? ""} ${(this.props.open ? "rotateY(125deg)" : "")}`}
				{...props as any}>
				<Box className={styles.handle} depth={this.props.depth ?? 20} height={this.props.height / 7} width={20}
					background={{
						all: "rgb(200, 200, 200)"
					}}
					style={{
						transform: `translateZ(${this.props.depth ?? 20}px)`
					}}
					customStyle={{
						all: {
							border: "1px solid black"
						}
					}}>
				</Box>
			</Box>
		)
	}
}