import React, { Component } from "react";
import Handler from "../../API/Handler";
import { IBox } from "../../API/Interfaces/IBox";
import { IFridge } from "../../API/Interfaces/IFridge";
import Box from "../Utility/Box/Box";
import ZoomedSampleBox from "../ZoomedSampleBox/ZoomedSampleBox";
import Door from "./Door/Door";
import styles from "./Fridge.module.css";
import { FridgeProps2 } from "./FridgeProps";
import Shelf from "./Shelf/Shelf";
import UnlocatedBoxes from "./UnlocatedBoxes/UnlocatedBoxes";

type OptionalBox = IBox | undefined;

export default class Fridge extends Component<FridgeProps2> {
	static get HEIGHT() {
		return window.innerHeight * 0.75;
	}
	static get WIDTH() {
		return window.innerHeight * 0.43;
	}
	static get DEPTH() {
		return window.innerHeight * 0.3;
	}

	fridgeRef = React.createRef<HTMLDivElement>();

	static FRIDGE_STRUCTURE = {
		shelves: 24,
		shelfGroups: 4,
		boxesX: 5,
		boxesZ: 5
	}

	state: {
		open: boolean;
		shelves: OptionalBox[][][]; // Array of y, x, z of boxes location. Note: Y ignores Shelf groups.
		zoomShelf?: number;
		unLocatedBoxes: IBox[];
		zoomedSampleProps?: ZoomedSampleBox["props"];
		currentDraggedBox?: IBox;
		shadowBox?: IBox;
		fridgeData?: IFridge;
		samplesColors?: any;
	} = {
			open: false,
			shelves: [],
			unLocatedBoxes: []
		};

	buildGridArray = (samples: IFridge) => {
		let shelves: IBox[][][] = new Array(Fridge.FRIDGE_STRUCTURE.shelves),
			unLocatedBoxes: IBox[] = [];
		for (let y = 0; y < Fridge.FRIDGE_STRUCTURE.shelves; y++) {
			let xes = new Array(Fridge.FRIDGE_STRUCTURE.boxesX);
			for (let x = 0; x < Fridge.FRIDGE_STRUCTURE.boxesX; x++) {
				let rows = new Array(Fridge.FRIDGE_STRUCTURE.boxesZ);
				for (let z = 0; z < Fridge.FRIDGE_STRUCTURE.boxesZ; z++) {
					let depth = undefined;
					rows[z] = depth;
				}
				xes[x] = rows;
			}
			shelves[y] = xes
		}

		Object.entries(samples).forEach(([key, value]) => {
			value = { ...value, name: key };
			if (value.location.x === null)
				unLocatedBoxes.push(value);
			else {
				let { x, y, z } = value.location;
				shelves[y][x][z] = value;
			}
		});
		this.setState({ shelves, unLocatedBoxes });
	};

	componentDidMount = async () => {
		const fridgeData = this.props.fridgeData ?? await Handler.getFridge();
		const samplesColors = await Handler.getSamplesColors();
		this.buildGridArray(fridgeData);
		this.setState({
			fridgeData,
			samplesColors
		});
		window.addEventListener("resize", this.onWindowResize);
	}

	componentWillUnmount() {
		window.removeEventListener("resize", this.onWindowResize);
	}

	onWindowResize = () => {
		this.forceUpdate();
	};

	detectExitDrag = (e: React.DragEvent) => {
		if (!this.fridgeRef.current?.contains(e.target as Node))
			this.setState({
				shadowBox: undefined
			});
	}

	render() {
		const height = Fridge.HEIGHT,
			width = Fridge.WIDTH,
			shadowBox = this.state.shadowBox,
			shelves = [...this.state.shelves.map(ele => [...ele.map(ele => [...ele])])];
		if (shadowBox)
			shelves[shadowBox.location.y][shadowBox.location.x][shadowBox.location.z] = shadowBox;
		return (
			<>
				<div
					ref={this.fridgeRef}
					className={
						styles.container +
						(this.props.className ? " " + this.props.className : "") +
						((this.state.zoomShelf ?? -1) >= 0 ? " " + styles.zoomShelf : "")
					}
					onDragLeave={this.detectExitDrag}
					style={{
						height,
						width,
						"--zoomedShelf": this.state.zoomShelf,
						"--numOfShelves": shelves.length
					} as any}>
					<Door
						height={height}
						width={width}
						transform={`translateZ(${Fridge.DEPTH / 2}px)`}
						background={{
							all: "rgb(56, 56 ,56)",
							back: "rgb(80, 80, 80)",
						}}
						customStyle={{
							all: {
								pointerEvents: "all",
							},
						}}
						open={this.state.open}
						onClick={this.onDoorClick} />
					<Box
						background={{
							back: "rgb(40, 40, 40)",
							top: "rgb(40, 40, 40)",
							bottom: "rgb(74, 74, 74)",
							left: "rgb(74, 74, 74)",
							right: "rgb(74, 74, 74)",
						}}
						depth={Fridge.DEPTH}
						height={height}
						width={width}
						customStyle={{
							all: {
								border: "1px solid black",
							},
						}} >
						<Shelf.Container className={(this.state.currentDraggedBox ? ' ' + styles.draggin : '')}>
							{
								this.splitToShelfGroups(shelves).map((shelfGroup: OptionalBox[][][], i: number) => (
									<div key={i} className={styles.shelfGroup}>
										{
											shelfGroup.map((row, j) => {
												const uniqueIndex = (Fridge.FRIDGE_STRUCTURE.shelves /
													Fridge.FRIDGE_STRUCTURE.shelfGroups) * i + j,
													isZoomned = this.state.zoomShelf === uniqueIndex;
												return (
													<Shelf
														onDragToColumnEnter={(col, row) => this.state.currentDraggedBox &&
															this.putShadowBox(this.state.currentDraggedBox, col, uniqueIndex, row)}
														key={j}
														onBoxDragStart={box => this.setState({ currentDraggedBox: box })}
														onBoxDragEnd={() => this.updateBoxLocation()}
														renderTubes={isZoomned}
														className={styles.shelf +
															(isZoomned ? ' ' + styles.zoomedShelf : '')}
														boxesGrid={row}
														onShelfClick={() => this.onShelfClick(uniqueIndex)}
														onBoxClick={this.handleBoxClick}
														width={width * 0.925}
														colors={this.state.samplesColors}
														height={height / shelves.length}
														depth={Fridge.DEPTH} />
												);
											})
										}
									</div>
								))
							}
						</Shelf.Container>
					</Box>
				</div>
				{
					this.state.zoomedSampleProps &&
					<ZoomedSampleBox
						colors={this.state.samplesColors}
						removeMe={() => this.setState({ zoomedSampleProps: undefined })}
						key={this.state.zoomedSampleProps.rect} {...this.state.zoomedSampleProps as any} />
				}
				<UnlocatedBoxes
					colors={this.state.samplesColors}
					onDragEnd={(i) => this.updateBoxLocation(i)}
					onDragStart={box => this.setState({
						currentDraggedBox: box
					})} boxes={this.state.unLocatedBoxes} className={styles.unsortedBoxes} />
			</>
		);
	}


	putShadowBox(box: IBox, x: number, y: number, z: number) {
		if (this.state.shelves[y][x].some(ele => !ele)) {
			box.location = {
				x,
				y,
				z
			};
			this.setState({
				shadowBox: box
			});
		}
	}

	updateBoxLocation = (indexInUnlocated?: number) => {
		const shadowBox = this.state.shadowBox;
		if (!shadowBox) {
			const currentDraggedBox = this.state.currentDraggedBox!;
			currentDraggedBox.location.x = null!;
			currentDraggedBox.location.y = null!;
			currentDraggedBox.location.z = null!;
			this.state.fridgeData![this.state.currentDraggedBox!.name] = currentDraggedBox;
			this.buildGridArray(this.state.fridgeData!);
			return this.setState({ currentDraggedBox: undefined });
		}
		const fridgeData = this.state.fridgeData!;
		fridgeData[shadowBox.name!] = shadowBox;
		this.buildGridArray(fridgeData);
		if (indexInUnlocated !== undefined)
			//eslint-disable-next-line
			this.state.unLocatedBoxes.splice(indexInUnlocated, 1);
		Handler.setBoxLocation(shadowBox);

		this.setState({
			shadowBox: undefined,
			currentDraggedBox: undefined
		});
	}

	splitToShelfGroups<T>(shelves: T[]): T[][] {
		const groupedArray = [],
			shelvesInGroup = Fridge.FRIDGE_STRUCTURE.shelves / Fridge.FRIDGE_STRUCTURE.shelfGroups;

		for (let shelfGroupIndex = 0; shelfGroupIndex < Fridge.FRIDGE_STRUCTURE.shelfGroups; shelfGroupIndex++) {
			let startIndex = shelfGroupIndex * shelvesInGroup;
			groupedArray.push(shelves.slice(startIndex, startIndex + shelvesInGroup));
		}
		return groupedArray
	}

	handleBoxClick: Shelf["props"]["onBoxClick"] = (e, box, props) => {
		this.setState({
			zoomedSampleProps: {
				box,
				rect: (e.target as Element).getBoundingClientRect(),
				...props
			}
		})
	}

	onDoorClick = () => {
		if (this.state.fridgeData) {
			if (Object.keys(this.state.fridgeData).length)
				this.setState({
					open: !this.state.open,
					zoomShelf: undefined,
				});
			else
				(window as any).displayToast({
					title: "Error!",
					body: "Something went wrong with the csv file! Maybe it doesn't exist?",
					id: "no csv?"
				});
		}
		else
			(window as any).displayToast({
				title: "Loading...",
				body: "Please wait, the fridge's data is still loading!",
				id: "loading"
			});
	};

	onShelfClick = (index: number) => {
		if (index === this.state.zoomShelf)
			this.setState({
				zoomShelf: undefined,
			});
		else
			this.setState({
				zoomShelf: index,
			});
	};
}
