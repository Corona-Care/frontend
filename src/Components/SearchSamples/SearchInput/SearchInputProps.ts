
export default interface SearchInputProps {
	onSearch?: (samples: Record<string, string>) => void;
	autoComplete?: Record<string, string[]>;
}
