import React, { Component } from "react";
import { Button, CloseButton, OverlayTrigger, Tooltip } from "react-bootstrap";
import { Typeahead } from "react-bootstrap-typeahead";
import { ReactComponent as SearchIcon } from "../../../Assets/icons/Search.svg";
import styles from "./SearchInput.module.css";
import SearchInputProps from "./SearchInputProps";

type AutoCompleteType = Record<string, string[]>;
type SearchFilter = Record<string, string>;

class SearchProp extends Component<{
	addFilter: (filter: SearchFilter) => void;
	prop: string;
	autoComplete?: AutoCompleteType;
	removeMe: () => void;
}> {
	state: { readOnly?: string } = {};
	render() {
		return (
			<div
				key={this.props.prop}
				className={styles.token}
				onClick={(e) => e.stopPropagation()}
				onFocus={(e) => e.stopPropagation()}>
				<p className={styles.propName}>{this.props.prop}:</p>
				{this.state.readOnly ? (
					<p className={styles.propValue}>
						{this.state.readOnly}
						<CloseButton onClick={() => this.props.removeMe()}
						/>
					</p>
				) : (
						<Typeahead
							autoFocus
							key={this.props.prop}
							onBlur={() => this.props.removeMe()}
							options={this.props.autoComplete?.[this.props.prop] ?? []}
							selectHintOnEnter
							onChange={(options) => this.addFilter(options[0])}
							minLength={0}
							id="search-sample"
							placeholder={"Search By " + this.props.prop}
						/>
					)}
			</div>
		);
	}

	addFilter = (option: string) => {
		this.props.addFilter({ [this.props.prop]: option });
		this.setState({
			readOnly: option,
		});
	};
};

class SearchFilters extends Component<{
	onChange: (filters: SearchFilter) => void;
	autoComplete?: AutoCompleteType
}> {
	state: { filters: SearchFilter } = {
		filters: {}
	};

	render() {
		return (
			<Typeahead
				options={Object.keys(this.props.autoComplete ?? {})}
				multiple
				selectHintOnEnter
				emptyLabel={Object.keys(this.props.autoComplete ?? {}).length ? "No property matched..." : "Loading....."}
				id="asd"
				renderToken={(option, props: any) => (
					<SearchProp
						removeMe={() => props.onRemove(option)}
						autoComplete={this.props.autoComplete}
						addFilter={this.addFilter} key={option} prop={option} />
				)}
			/>
		);
	}

	addFilter = (filter: SearchFilter) => {
		const filters = this.state.filters;
		Object.entries(filter).forEach(([key, val]) => filters[key] = val);
		this.setState({ filters });
		this.props.onChange(this.state.filters);
	};
}

export default class SearchInput extends Component<SearchInputProps> {
	state: { filters?: SearchFilter } = {};

	render() {
		return (
			<div className={styles.container}>
				<SearchFilters onChange={this.setFilters} autoComplete={this.props.autoComplete} />
				<OverlayTrigger
					overlay={<Tooltip id="submit search">Submit Search</Tooltip>}
					placement="top">
					<Button
						variant="light"
						className="mx-auto d-block"
						onClick={this.doSearch}>
						<SearchIcon />
					</Button>
				</OverlayTrigger>
			</div>
		);
	}

	setFilters = (filters: SearchFilter) => this.setState({ filters });

	doSearch = async () => {
		this.props.onSearch?.(this.state.filters ?? {});
	};
}
