import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import APIHandler from "../../API/Handler";
import { ISampleData } from "../../API/Interfaces/ISample";
import ResultsTable from "../Utility/ResultsTable/ResultsTable";
import SearchInput from "./SearchInput/SearchInput";
import SampleModal from "../Utility/SampleModal/SampleModal";
import styles from "./SearchSamples.module.css";

export class SearchSamples extends Component {
	state: {
		filterdSamples?: ISampleData[];
		allSamples: [];
		autoCompleteData?: Record<string, string[]>
		modalVisible: boolean;
		selectedSample?: ISampleData;
	} = {
			allSamples: [],
			modalVisible: false,
		};

	componentDidMount = async () => {
		const results = await APIHandler.getFridge(),
			allSamples: ISampleData[] = [],
			autoCompleteDataSet: Record<string, Set<string>> = {},
			autoCompleteData: Record<string, string[]> = {};

		Object.values(results).forEach(value => {
			let samples = value.samples;
			Object.values(samples).forEach((value) => {
				allSamples.push(value);

				// populate autoCompleteData Object
				Object.entries(value).forEach(([key, val]) => {
					if (!val) return;
					if (key in autoCompleteDataSet)
						autoCompleteDataSet[key].add(val.toString());
					else
						autoCompleteDataSet[key] = new Set([val.toString()]);
				});
			});
		});
		Object.entries(autoCompleteDataSet).forEach(([key, val]) => autoCompleteData[key] = Array.from(val.values()));


		this.setState({ allSamples, autoCompleteData });
	};

	searchSamlpes = (params: Record<string, string>) => {
		let filterdSamples: ISampleData[] = [];
		let allSamples = this.state.allSamples;

		allSamples.forEach((value) => {
			if (this.isValidSample(value, params)) {
				filterdSamples.push(value);
			}
		});

		this.setState({ filterdSamples });
	};

	isValidSample = (sample: ISampleData, params: any) => {
		let valid = true;
		Object.keys(params).forEach((key) => {
			Object.entries(sample).forEach(([sampleKey, value]) => {
				if (sampleKey.toString() === key.toString()) {
					if (value.toString() !== params[key].toString()) valid = false;
				}
			});
		});
		return valid;
	};

	handleSampleClick = (sample: ISampleData) => {
		this.setState({ modalVisible: true, selectedSample: sample });
	};
	render() {
		const { filterdSamples, modalVisible } = this.state;
		return (
			<>
				{this.state.selectedSample && (
					<SampleModal
						visible={modalVisible}
						sampleCode={this.state.selectedSample.barcode.toString()}
						sample={this.state.selectedSample}
						onClose={() => this.setState({ modalVisible: false })}
					/>
				)}
				<Modal.Dialog size="xl">
					<Modal.Header>
						<Modal.Title>Search for a sample!</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<SearchInput autoComplete={this.state.autoCompleteData} onSearch={this.searchSamlpes} />
					</Modal.Body>
				</Modal.Dialog>

				<Modal.Dialog size="xl" className={styles.resultsTable}>
					<Modal.Header>
						<Modal.Title>Results</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						{
							<ResultsTable
								onSampleClick={this.handleSampleClick}
								samples={filterdSamples}
							/>
						}
					</Modal.Body>
				</Modal.Dialog>
			</>
		);
	}
}
