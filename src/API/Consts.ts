
export default class Consts {
	static server = process.env.REACT_APP_API_ENDPOINT || ''
	static api = '/api/v1/'
	static fridgeRoute = `${Consts.api}boxes`
	static sampleRoute = `${Consts.api}samples/`
	static patientRoute = `${Consts.api}patient/`
	static csvRoute = `${Consts.api}csv`
	static updateBoxLocation = `${Consts.api}box/location/`
	static colorsRoute = `${Consts.sampleRoute}colors`
	static autoCompleteRoute = `${Consts.api}sample/autocomplete`
	static csvDownloadRoute = `${Consts.api}boxes/location`
}