import { ISampleData } from "./ISample";

export interface IBox {
	name: string;
	location: { x: number, y: number, z: number };
	samples: { [key: string]: ISampleData };
	size: number;
}