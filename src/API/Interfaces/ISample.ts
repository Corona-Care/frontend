import { IPatient } from './IPaitent';

export interface ISample {
	id: string,
	pos_x: Number,
	pos_y: Number,
	patient: IPatient
}

export interface ISampleData {
	type: string
	barcode: string | number
	is_removed: number
	shipped_dt: any
	shipped_txt: any
	box: number
	box_id: string
	box_size: number
	cell: number
	study_num: string
}