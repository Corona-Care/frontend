import { ISession } from "./ISession";

export interface IPatient {
	id: string;
}

export interface IPatientData {
	ethnic: Array<string>;
	healthcare_personnel: string;
	place_living: Array<string>;
	religions: Array<string>;
	sessions: Array<ISession>;

}