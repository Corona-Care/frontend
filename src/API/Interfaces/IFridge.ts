import { IBox } from "./IBox";

export interface IFridge {
	[key: string]: IBox;
}