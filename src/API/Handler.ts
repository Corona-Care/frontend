import Consts from './Consts';
import { IBox } from "./Interfaces/IBox";
import { IFridge } from "./Interfaces/IFridge";

export default new class APIHandler {
	fridgeData?: IFridge;

	async getFridge(): Promise<IFridge> {
		if (this.fridgeData)
			return this.fridgeData;

		let response = await fetch(Consts.server + Consts.fridgeRoute);
		let data = await response.json();
		this.fridgeData = data;
		return data
	}

	async getSearchAutocompletes(): Promise<{ [key: string]: (string | number | null)[] }> {
		const response = await fetch(Consts.server + Consts.autoCompleteRoute);
		return await response.json()
	}

	async uploadCsv(file: File) {
		const formData = new FormData();
		formData.append('file', file);
		let response = await fetch(Consts.server + Consts.csvRoute, {
			method: 'POST',
			body: formData
		});
		let data = await response.json();
		return data;
	}

	async getSampleData(sampleID: string) {
		let response = await fetch(Consts.server + Consts.sampleRoute + sampleID);
		let data = await response.json();
		return data
	}

	async getPatientData(patientId: string) {
		let response = await fetch(Consts.server + Consts.patientRoute + patientId);
		let data = await response.json();
		return data
	}
	async setBoxLocation(box: IBox) {
		const data = await fetch(Consts.server + Consts.updateBoxLocation + box.name, {
			method: "POST",
			body: JSON.stringify(box.location),
			headers: {
				'Content-Type': 'application/json',
			}
		});
		return (await data.json()).success;
	}

	async getSamplesColors() {
		let response = await fetch(Consts.server + Consts.colorsRoute);
		let data = await response.json();
		return data
	}

	async getHome() {
		let response = await fetch(Consts.server);
		let data = await response.json();
		return data;
	}
}();