import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { HashRouter, Route } from "react-router-dom";
import "../node_modules/react-bootstrap/";
import './App.css';
import Background from './Components/Background/Background';
import Fridge from './Components/Fridge/Fridge';
import Home from './Components/Home/Home';
import NavBar from './Components/NavBar/NavBar';
import { SearchSamples } from './Components/SearchSamples/SearchSamples';
import ToastMessages from './Components/ToastMessages/ToastMessages';

function App() {
	return (
		<HashRouter>
			<NavBar />
			<Background />
			<div className="content">
				<Route path="/about" exact component={Home} />
				<Route path="/search" exact component={SearchSamples} />
				<Route path="/fridge" exact component={Fridge} />
				<Route path="/" exact component={Fridge} />
			</div>
			<ToastMessages />
		</HashRouter>
	);
}

export default App;
